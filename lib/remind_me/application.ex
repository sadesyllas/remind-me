defmodule RemindMe.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      RemindMe.Scheduler,
      RemindMe
    ]

    opts = [strategy: :one_for_one, name: RemindMe.Supervisor]

    Supervisor.start_link(children, opts)
  end
end
