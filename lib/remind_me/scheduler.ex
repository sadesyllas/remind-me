defmodule RemindMe.Scheduler do
  @moduledoc false

  @module __MODULE__

  alias RemindMe.Reminder
  alias RemindMe.Scheduler.Backend

  use GenServer

  def start_link(_) do
    GenServer.start_link(@module, %{}, name: @module)
  end

  def init(state) do
    {:ok, state}
  end

  def schedule(options), do: GenServer.call(@module, {:schedule, options}, :infinity)

  def handle_call({:schedule, options}, _from, state) do
    reply =
      with {:ok, reminder} <- Reminder.parse(options) do
        Backend.process(reminder)
      end

    {:reply, reply, state}
  end
end
