defmodule RemindMe.Reminder do
  @moduledoc false

  alias __MODULE__

  defstruct [:at, :message, :target, :every]

  @type t :: %Reminder{
          at: tuple,
          message: binary,
          target: binary,
          every: number
        }

  def parse(options) when is_list(options) do
    at = Keyword.get(options, :at)
    message = Keyword.get(options, :message)
    target = Keyword.get(options, :target)
    every = Keyword.get(options, :every, 0)

    with {:ok, at} <- parse(:at, at),
         {:ok, message} <- parse(:message, message),
         {:ok, target} <- parse(:target, target),
         {:ok, every} <- parse(:every, every) do
      reminder = %Reminder{
        at: at,
        message: message,
        target: target,
        every: every
      }

      {:ok, reminder}
    end
  end

  def parse(options), do: {:error, "Unparsable options: #{inspect(options)}"}

  defp parse(:at, {hour, minute, second})
       when is_number(hour) and hour >= 0 and hour <= 23 and is_number(minute) and minute >= 0 and
              minute <= 59 and is_number(second) and second >= 0 and second <= 59 do
    now = DateTime.utc_now()
    at = {now.year, now.month, now.day, hour, minute, second}

    {:ok, at}
  end

  defp parse(:at, {year, month, day, hour, minute, second} = at)
       when is_number(year) and is_number(month) and month >= 1 and month <= 12 and is_number(day) and
              day >= 1 and day <= 31 and is_number(hour) and hour >= 0 and hour <= 23 and
              is_number(minute) and minute >= 0 and minute <= 59 and is_number(second) and
              second >= 0 and second <= 59 do
    cond do
      DateTime.utc_now().year > year ->
        {:error, "Year #{year} is in the past"}

      Calendar.ISO.valid_date?(year, month, day) and
          Calendar.ISO.valid_time?(hour, minute, second, 0) ->
        {:ok, at}

      true ->
        {:error, "Unparsable date/time: #{inspect(at)}"}
    end
  end

  defp parse(:at, at), do: {:error, "Unparsable date/time: #{inspect(at)}"}

  defp parse(:message, message) when is_binary(message) do
    if Regex.replace(~r/\s+/, message, "", global: true) === "",
      do: {:error, "Empty message"},
      else: {:ok, message}
  end

  defp parse(:message, message), do: {:error, "Unparsable message: #{inspect(message)}"}

  defp parse(:target, <<"<", marker>> <> rest) when marker in [?#, ?@] do
    rest = rest |> String.replace_trailing(">", "") |> Integer.parse() |> elem(0)
    type = if marker === ?#, do: :channel, else: :user

    {:ok, {type, rest}}
  end

  defp parse(:target, target), do: {:error, "Unparsable target: #{inspect(target)}"}

  defp parse(:every, every) when every in [:once, :daily, :weekly, :monthly, :yearly] do
    {:ok, every}
  end

  defp parse(:every, every), do: {:error, "Unparsable every: #{inspect(every)}"}
end
