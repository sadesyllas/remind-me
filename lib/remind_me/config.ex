defmodule RemindMe.Config do
  @moduledoc false

  def store(config), do: config

  def load(), do: nil
end
