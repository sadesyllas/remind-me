defmodule RemindMe.Backend do
  @moduledoc false

  alias RemindMe.Scheduler

  def process(%{target: <<prefix>> <> _ = target, recur: recur, at: at, message: message})
      when (target === "me" or prefix in [?@, ?#]) and
             recur in [:once, :daily, :weekly, :monthly, :yearly] do
    #
  end

  def process(options), do: {:error, "Incomplete options: #{inspect(options)}"}

  defp schedule(options) when is_list(options) do
    with {:ok, reminder} <- Scheduler.schedule(options) do
      {:ok, inspect(reminder)}
    end
  end

  defp schedule(options), do: {:error, "Unparsable options: #{inspect(options)}"}
end
