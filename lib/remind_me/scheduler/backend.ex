defmodule RemindMe.Scheduler.Backend do
  @moduledoc false

  alias RemindMe.Reminder

  def process(%Reminder{} = reminder) do
    {:ok, reminder}
  end
end
