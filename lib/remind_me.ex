defmodule RemindMe do
  @moduledoc """
  \\remind @|#... once|daily|... YYYY/MM/DD-HH:mm:ss|15m|1w2d4h|... blah blah blah...
  \\remind list
  \\remind rm
  \\remind tz Europe/Athens
  """

  require Logger

  @module __MODULE__

  @regex ~r/^\s*\\remind\s+(?<target>me|<[@#].+?>)\s+(?<recur>once|daily|weekly|monthly|yearly)\s+(?<at>[0-9]{4}\/[0-9]{1,2}\/[0-9]{1,2}-[0-9]{1,2}:[0-9]{1,2}:[0-9]{1,2}|(?:[0-9]+[yMdwhms])+)\s+(?<message>.+)$/s

  use Nostrum.Consumer

  alias Nostrum.Api
  alias RemindMe.Backend

  @spec start_link() :: no_return
  def start_link(), do: Consumer.start_link(@module)

  def regex(), do: @regex

  @impl true
  def handle_event({:MESSAGE_CREATE, {msg}, _state}) do
    msg = %{msg | content: String.trim(msg.content)}

    # IO.inspect(msg, label: "koukou")

    with true <- Regex.match?(@regex, msg.content),
         {:ok, message} <- @regex |> Regex.named_captures(msg.content) |> Backend.process() do
      IO.inspect(message)
      # Api.create_message(msg.channel_id, message)
    else
      {:error, reason} ->
        Logger.error(reason)

        :ignore

      _ ->
        :ignore
    end
  end

  @impl true
  def handle_event(_event), do: :noop
end
