defmodule RemindMe.MixProject do
  use Mix.Project

  def project do
    [
      app: :remind_me,
      version: "0.1.0",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  def application do
    [
      extra_applications: [:logger],
      mod: {RemindMe.Application, []}
    ]
  end

  defp deps do
    [
      {:jason, "~> 1.1"},
      {:nostrum, git: "https://github.com/Kraigie/nostrum.git"}
    ]
  end
end
