use Mix.Config

config :nostrum,
  token: "",
  num_shards: :auto

secrets_file_name = "secrets.exs"
secrets_file_path = Path.join(__DIR__, secrets_file_name)

if File.exists?(secrets_file_path), do: import_config(secrets_file_name)
